<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- CONTENT -->
			<section class="content-demo">
				<div class="container">
					<div class="content-title">
						<h2><?php the_archive_title(); ?></h2>
					</div>
					<div class="demo">
						<div class="row">
							<?php

								if ( have_posts() ) :

									while ( have_posts() ) : the_post();

										get_template_part( 'template-parts/content', 'themes' );

									endwhile;

								endif;
							?>
						</div>

						<?php pd_posts_pagination(); ?>

					</div>

				</div>
			</section>
			<!-- END CONTENT -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
