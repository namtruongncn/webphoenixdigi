(function($) {
    "use strict";

    // backtop functions
    $( '#backtotop' ).on( 'click', function () {
      $( 'html, body' ).animate( { scrollTop: 0 }, 800 );
      return false;
    });

    $( window ).on( 'scroll', function () {
      if ( $( this ).scrollTop() > 100 ) {
        $( '#backtotop' ).fadeIn( 1000, function() {
          $( 'span' , this ).fadeIn( 100 );
        });
      } else {
        $( '#backtotop' ).fadeOut( 1000, function() {
          $( 'span' , this ).fadeOut( 100 );
        });
      }
    });
    // end function backtop

    $('.mobile-menu .menu-item-has-children, .vertical-mega-mobile-menu .menu-item-has-children').prepend('<i class="fa fa-angle-down"></i>');

    $('.mobile-menu .menu-item-has-children > i, .vertical-mega-mobile-menu .menu-item-has-children > i').click(function(event) {
      $(this).parent().toggleClass('active');
    });

    $('#menu-toggle, .mobile-menu-container .close-menu').click(function(event) {
       $('.site').toggleClass('mobile-menu-active');
    });

    $('#mega-menu-toggle, .vertical-mega-mobile-menu .close-menu').click(function(event) {
       $('.site').toggleClass('vertical-mega-mobile-menu-active');
    });

    $('.vertical-mega-mobile-menu-active .overlay').click(function(event) {
        $('.site').removeClass('vertical-mega-mobile-menu-active');
    });

    $('.mobile-menu-active .overlay').click(function(event) {
        $('.site').removeClass('mobile-menu-active');
    });

    $('.fixed-area .widget_custom_html .widget-title').click(function(event) {
        $('.fixed-area .fb-page').toggleClass('hide');
    });

    // $('').click(function(event) {
    //     // $('.fb-page').toggleClass('hide');
    // });

    $('.box-domain-search').on('submit', function(){
        var domain = $(this).find('.domain-name').val();

        if ( ! domain ) {
            alert( 'Vui lòng nhập tên miền' );
            return false;
        }

        $('.loading').removeClass('hide').addClass('show');

        $('#result').html('');

        var data = {
            'action': 'pd_display',
            'domain': domain,
            'security' : pd_ajax.pd_nonce
        };

        $.post(pd_ajax.ajax_url, data, function(response){
            $('.loading').removeClass('show').addClass('hide');
            $('#result').html(response);
        });

        return false;

        // e.preventDefault();
    });

})(jQuery);
