<?php
/**
 * Trang chủ template
 * Description: Template for home page.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

get_header( 'fullwidth' ); ?>

	<!-- SLIDER -->
	<div class="phoenix-slider">
		<?php echo do_shortcode( pd_option( 'header_slider', null, false ) ); ?>
	</div>
	<!-- END SLIDER -->

	<?php get_template_part( 'template-parts/front-page/domain' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- CONTENT -->
			<section class="content-demo">
				<div class="container">
					<div class="content-title">
						<h2>Các Mẫu Giao Diện Tham Khảo</h2>
					</div>
					<div class="demo">
						<div class="row">
							<?php
								$themes_query = new WP_Query( array(
									'post_type'           => 'themes',
									'posts_per_page'      => 12,
									'ignore_sticky_posts' => 1,
								) );

								if ( $themes_query->have_posts() ) :
									while ( $themes_query->have_posts() ) : $themes_query->the_post();

										get_template_part( 'template-parts/content', 'themes' );

									endwhile;
									wp_reset_postdata();
								endif;
							?>
							<div class="col-md-12 text-center">
								<a class="project_view-all" href="<?php echo esc_url( get_post_type_archive_link( 'themes' ) ); ?>"><i class="fa fa-external-link" aria-hidden="true"></i> <?php esc_html_e( 'Xem tất cả', 'phoenixdigi' ); ?></a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- END CONTENT -->

			<?php get_template_part( 'template-parts/front-page/testimonials' ); ?>

			<?php get_template_part( 'template-parts/front-page/support' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer( 'fullwidth' );
