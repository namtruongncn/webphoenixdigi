<?php
/**
 * Template Name: Liên Hệ
 */
get_header( 'fullwidth' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="page-title">
				<?php the_title( '<h1 class="heading">', '</h1>' ); ?>
				<div class="desc">CHÚNG TÔI SẼ GIÚP BẠN CÓ NGAY WEBSITE CHUYÊN NGHIỆP NHẤT</div>
			</div>
			<section class="lienhe">
				<div class="container">
					<div class="row">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<div id="breadcrumbs" class="col-md-12">','</div>');
						} ?>
						<div class="col-md-12">
							<div class="map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.8497308377646!2d105.80978621548593!3d20.998659886014682!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac90b04d61fd%3A0x6df305db960277cb!2sSapphire+Palace!5e0!3m2!1svi!2s!4v1496630487385" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="col-md-12 contact_address">
							<div class="row">
								<div class="contact_info text-center col-md-4">
									<div class="contact_icon">
										<i class="fa fa-map"></i>
									</div>
									<p>Phòng 2001, tầng 20, Tòa nhà Sapphire Palace<br /> số 4 Chính Kinh, Thanh Xuân, Hà Nội</p>
								</div>
								<div class="contact_info text-center col-md-4">
									<div class="contact_icon">
										<i class="fa fa-phone"></i>
									</div>
									<p>0968 716 123<br />098 463 8060 - 01639 482 057</p>
								</div>
								<div class="contact_info text-center col-md-4">
									<div class="contact_icon">
										<i class="fa fa-envelope"></i>
									</div>
									<p>contact@phoenixdigi.vn<br />kythuat@phoenixdigi.vn</p>
								</div>
							</div>
						</div>
						<div class="col-md-12 contact_form">
							<?php echo do_shortcode( get_the_content() ); ?>
						</div>
					</div>
				</div>
			</section>
		</main>
	</div>

<?php
get_footer( 'fullwidth' );