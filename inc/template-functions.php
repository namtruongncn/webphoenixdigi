<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function pd_body_classes( $classes ) {
	// Add class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Add class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Add class if we're viewing the Customizer for easier styling of theme options.
	if ( is_customize_preview() ) {
		$classes[] = 'pd-customizer';
	}

	// Add class on front page.
	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'pd-front-page';
	}

	// Add a class if there is a custom header.
	if ( has_header_image() ) {
		$classes[] = 'has-header-image';
	}

	// Add class if sidebar is used.
	if ( is_active_sidebar( 'sidebar-1' ) && ! is_page() ) {
		$classes[] = 'has-sidebar';
	}

	// Add class if the site title and tagline is hidden.
	if ( 'blank' === get_header_textcolor() ) {
		$classes[] = 'title-tagline-hidden';
	}

	if ( pd_option( 'vertical_mega_menu', null, false ) && is_active_sidebar( 'vertical-mega-menu' ) ) {
		$classes[] = 'has-vertical-mega-menu';
	}

	return $classes;
}
add_filter( 'body_class', 'pd_body_classes' );

/**
 * Adds custom classes to the array of layout classes.
 *
 * @param array $classes Classes for the layout element.
 */
function pd_layout_class( $classes = array() ) {
	$classes = (array) $classes;

	if ( PD_Sidebar::has_sidebar() ) {
		$classes[] = sprintf( 'sidebar-%s', PD_Sidebar::get_sidebar_area() );
	} else {
		$classes[] = 'no-sidebar';
	}

	$classes = apply_filters( 'pd_layout_class', $classes );

	echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
}

/**
 * Primary menu fallback function.
 */
function primary_menu_fallback() {
	$classes = pd_option( 'enable_header_search', null, false ) ? 'primary-menu-container visible-lg col-md-9' : 'primary-menu-container visible-lg col-md-12';

	$fallback_menu = '<div class="' . $classes . '"><ul id="primary-menu" class="menu clearfix"><li><a href="%1$s" rel="home">%2$s</a></li></ul></div>';
	printf( $fallback_menu, esc_url( home_url( '/' ) ), esc_html__( 'Trang chủ', 'phoenixdigi' ) ); // WPCS: XSS OK.
}

/**
 * Mobile menu fallback function.
 */
function mobile_menu_fallback() {
	$fallback_menu = '<ul id="mobile-menu" class="mobile-menu"><li><a href="%1$s" rel="home">%2$s</a></li></ul>';
	printf( $fallback_menu, esc_url( home_url( '/' ) ), esc_html__( 'Trang chủ', 'phoenixdigi' ) ); // WPCS: XSS OK.
}

// Fix Seo by yoast
add_filter( 'wpseo_canonical', '__return_false' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since PD Theme 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function pd_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<a class="link-more" href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Đọc thêm &raquo;<span class="screen-reader-text"> "%s"</span>', 'phoenixdigi' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'pd_excerpt_more' );

/**
 * Disable WP emojicons.
 */
function disable_wp_emojicons() {

	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	add_filter( 'emoji_svg_url', '__return_false' );
}
add_action( 'init', 'disable_wp_emojicons' );

/**
 * Disable WP emojicons.
 *
 * @param  array $plugins //
 * @return array          //
 */
function disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove script version.
 *
 * @param  string $src //
 * @return string      //
 */
function pd_remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', 'pd_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'pd_remove_script_version', 15, 1 );

if ( pd_option( 'include_fb_sdk_js', null, false ) ) {
	/**
	 * Include Facebook App ID on footer.
	 */
	function pd_include_facebook_app_id() {
		?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/<?php pd_option( 'fb_language' ); ?>/sdk.js#xfbml=1&version=v2.7&appId=<?php pd_option( 'facebook_app_id' ); ?>";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<?php
	}
	add_action( 'wp_footer', 'pd_include_facebook_app_id' );
}

/**
 * Rt layout classes.
 *
 * @param array $classes CSS selector for own site.
 */
function pd_layout_classes( $classes = array() ) {
	$classes = (array) $classes;

	if ( 'boxed' == pd_option( 'site_layout', 'full', false ) ) {
		$classes[] = 'boxed';
	} else {
		$classes[] = 'full';
	}

	if ( '1000' == $site_width = pd_option( 'site_width', '1000', false ) ) {
		$classes[] = 'w1000';
	} elseif ( '1170' == $site_width ) {
		$classes[] = 'w1170';
	} elseif ( '1200' == $site_width ) {
		$classes[] = 'w1200';
	} else {
		$classes[] = 'w1000';
	}

	$classes = apply_filters( 'pd_layout_classes', $classes );

	echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
}

/**
 * Excerpt lenght.
 *
 * @param  int $length Number to change excerpt length.
 * @return int         //
 */
function pd_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'pd_excerpt_length', 999 );

/**
 * Css for our theme.
 *
 * @return string Css
 */
function pd_customizer_css_option() {
	$css= '';

	if ( '#ea2b33' != pd_option( 'main_bg_color', null, false ) ) {
		$css = '.main-navigation, .widget-title, .site-footer, .top-footer, .copyright, .close-menu {background:' . pd_option( 'main_bg_color', null, false ) . ';}';
		$css .= '.header-search .search-form .search-submit {color:' . pd_option( 'main_bg_color', null, false ) . ';}';
	}

	if ( $gutter_width = pd_option( 'gutter_width', null, false ) ) {
		$css .= ".rt__grid_products .product{padding-left:{$gutter_width}px;padding-right:{$gutter_width}px;}";
		$css .= ".rt__grid_products.row{margin-left:-{$gutter_width}px;margin-right:-{$gutter_width}px;}";
	}

	if ( $submenu_bg_color = pd_option( 'submenu_bg_color', null, false ) ) {
		$css .= "#primary-menu li ul.sub-menu{background:{$submenu_bg_color}}";
	}

	wp_add_inline_style( 'rt-main', $css );
}
add_action( 'wp_enqueue_scripts', 'pd_customizer_css_option', 11 );

// Support shortcodes in rt textarea widgets
add_filter('pd_textarea_widget', 'do_shortcode');

/**
 * Display back to top text
 *
 * @param  boolean $echo Echo or return value of back to top button.
 * @return string        Back to top html.
 */
function pd_back_to_top( $echo = true ) {
	if ( ! $backtotop = pd_option( 'totop', true, false ) ) {
		return;
	}

	$btt_text = '<div id="backtotop" title="' . esc_html__( 'Lên đầu trang', 'phoenixdigi' ) . '"></div>';

	$btt_text = apply_filters( 'pd_back_to_top_html', $btt_text );

	if ( ! $echo ) {
		return $btt_text;
	}

	print $btt_text; // WPCS: XSS OK.
}

if ( pd_option( 'header_script', null, false ) && pd_option( 'header_script_on_off', null, false ) ) {
/**
 * Adds script on wp_head.
 *
 * @return string //
 */
function pd_header_script() {
	pd_option( 'header_script' );
}
add_action( 'wp_head', 'pd_header_script' );
}

if ( pd_option( 'footer_script', null, false ) && pd_option( 'footer_script_on_off', null, false ) ) {
/**
 * Adds script on wp_footer.
 *
 * @return string //
 */
function pd_footer_script() {
	pd_option( 'footer_script' );
}
add_action( 'wp_footer', 'pd_footer_script' );
}


function pd_display_func() {
	check_ajax_referer( 'pd_nonce', 'security' );

	if ( isset( $_POST['domain'] ) ) {

		$domain = str_replace( array( 'www.', 'http://'), NULL, $_POST['domain'] );
		$split = explode( '.', $domain );
			// var_dump( $split );

		$domains = array(
			'com' => $split[0] . '.com',
			'vn' => $split[0] . '.vn',
			'comvn' => $split[0] . '.com.vn',
			'net' => $split[0] . '.net',
			'eduvn' => $split[0] . '.edu.vn',
		);

		if ( 1 == count( $split ) ) {

			foreach ( $domains as $key => $domain ) {

				$domain = preg_replace( '/[^-a-zA-Z0-9.]+/', '', $domain );

				if ( strlen( $domain ) > 0 ) {
					$search = file_get_contents( 'https://vnwho.com/api/' . $domain . '/' );

					$search = json_decode( $search, true );

					if ( 'whois' == $search['status'] ) {
						echo '<div class="alert alert-warning">';
							echo '<div><i class="fa fa-times" aria-hidden="true"></i> Tên miền <i>' . $domain . '</i><strong> đã được đăng ký.</strong></div>';
						echo '</div>';
					} else {
						echo '<div class="alert alert-success">';
							echo '<div><i class="fa fa-check" aria-hidden="true"></i> Tên miền <i>' . $domain . '</i><strong> chưa được đăng ký.</strong></div>';
						echo '</div>';
					}
				}
			}
		} else {
			$domain = preg_replace( '/[^-a-zA-Z0-9.]+/', '', $domain );

			if ( strlen( $domain ) > 0 ) {
				$search = file_get_contents( 'https://vnwho.com/api/' . $domain . '/' );

				$search = json_decode( $search, true );

				if ( 'whois' == $search['status'] ) {
					echo '<div class="alert alert-warning">';
						echo '<div><i class="fa fa-times" aria-hidden="true"></i> Tên miền <i>' . $domain . '</i><strong> đã được đăng ký.</strong></div>';
					echo '</div>';
				} else {
					echo '<div class="alert alert-success">';
							echo '<div><i class="fa fa-check" aria-hidden="true"></i> Tên miền <i>' . $domain . '</i><strong> chưa được đăng ký.</strong></div>';
						echo '</div>';
				}
			}
		}
	}

	die();
}
add_action('wp_ajax_pd_display','pd_display_func');
add_action('wp_ajax_nopriv_pd_display','pd_display_func');

/**
 * [pd_posts_pagination description]
 * @return [type] [description]
 */
function pd_posts_pagination() {
	the_posts_pagination( array(
		'prev_text' => '<span class="screen-reader-text">' . esc_html__( 'Trang trước', 'phoenixdigi' ) . '</span>&laquo;',
		'next_text' => '<span class="screen-reader-text">' .  esc_html__( 'Trang sau', 'phoenixdigi' ) . '</span>&raquo;',
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Trang', 'phoenixdigi' ) . ' </span>',
	) );
}

/**
 * Retrieve the archive title based on the queried object.
 *
 * @since 4.1.0
 *
 * @return string Archive title.
 */
function pd_get_the_archive_title() {
	if ( is_category() ) {
		/* translators: Category archive title. 1: Category name */
		$title = sprintf( __( '%s' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		/* translators: Tag archive title. 1: Tag name */
		$title = sprintf( __( '%s' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		/* translators: Author archive title. 1: Author name */
		$title = sprintf( __( '%s' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		/* translators: Yearly archive title. 1: Year */
		$title = sprintf( __( '%s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
	} elseif ( is_month() ) {
		/* translators: Monthly archive title. 1: Month name and year */
		$title = sprintf( __( '%s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
	} elseif ( is_day() ) {
		/* translators: Daily archive title. 1: Date */
		$title = sprintf( __( '%s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title' );
		}
	} elseif ( is_post_type_archive() ) {
		/* translators: Post type archive title. 1: Post type name */
		$title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives' );
	}

	/**
	 * Filters the archive title.
	 *
	 * @since 4.1.0
	 *
	 * @param string $title Archive title to be displayed.
	 */
	return apply_filters( 'pd_get_the_archive_title', $title );
}
add_filter( 'get_the_archive_title', 'pd_get_the_archive_title' );
