<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * @see https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/example.php
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 */

/**
 * Register the required plugins for this theme.
 */
function pd_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 */
	$plugins = array(
		// array(
		// 	'name'     => esc_html__( 'RT Core', 'phoenixdigi' ),
		// 	'slug'     => 'rt-plugin',
		// 	'source'   => 'https://github.com/namncn/rt-plugin/archive/master.zip',
		// 	'required' => true,
		// ),
		array(
			'name'     => esc_html__( 'Meta Slider', 'phoenixdigi' ),
			'slug'     => 'ml-slider',
			'required' => true,
		),
		// array(
		// 	'name'     => esc_html__( 'WooCommerce', 'phoenixdigi' ),
		// 	'slug'     => 'woocommerce',
		// 	'required' => true,
		// ),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 */
	$config = array(
		'id'           => 'phoenixdigi',
		'is_automatic' => true,
		'strings'      => array( 'nag_type' => 'notice-warning' ),
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'pd_register_required_plugins' );
