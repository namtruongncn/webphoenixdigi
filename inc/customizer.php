<?php
/**
 * PD Theme Customizer.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function pd_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector' => '.site-title a',
			'container_inclusive' => false,
			'render_callback' => 'pd_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector' => '.site-description',
			'container_inclusive' => false,
			'render_callback' => 'pd_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'pd_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Phoenix_Digi 1.0.0
 * @see pd_customize_register()
 *
 * @return void
 */
function pd_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Phoenix_Digi 1.0.0
 * @see pd_customize_register()
 *
 * @return void
 */
function pd_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function pd_customize_preview_js() {
	wp_enqueue_script( 'pd_customizer', get_theme_file_uri( 'assets/js/customizer.js' ), array( 'customize-preview' ), '1.0.0', true );
}
add_action( 'customize_preview_init', 'pd_customize_preview_js' );

/**
 * PD_Customizer_Manager Class.
 */
final class PD_Customizer_Manager {
	/**
	 * Register settings for the Theme Customizer.
	 */
	public static function init() {
		add_action( 'customize_register', array( __CLASS__, 'register' ) );
	}

	/**
	 * Register settings for the Theme Customizer.
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register( WP_Customize_Manager $wp_customize ) {
		static::register_site_layout( $wp_customize );
		static::register_background( $wp_customize );
		static::register_script( $wp_customize );
		static::register_facebook( $wp_customize );
		static::register_footer( $wp_customize );
		static::register_general_option( $wp_customize );
	}

	/**
	 * Register site logo setting for the Theme Customizer
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_site_layout( WP_Customize_Manager $wp_customize ) {
		// Site logo settings.
		$wp_customize->add_setting( 'site_layout' , array(
			'default'           => pd_default( 'site_layout' ),
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'site_layout', array(
			'section'   => 'title_tagline',
			'type'      => 'radio',
			'label'     => esc_html__( 'Chọn layout của giao diện', 'phoenixdigi' ),
			'priority'  => 50,
			'choices'   => array(
				'boxed' => esc_html__( 'Dạng thu gọn', 'phoenixdigi' ),
				'full'  => esc_html__( 'Dạng tràn chiều rộng', 'phoenixdigi' ),
			),
		) );

		// Site width settings.
		$wp_customize->add_setting( 'site_width' , array(
			'default'           => pd_default( 'site_width' ),
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'site_width', array(
			'section'        => 'title_tagline',
			'type'           => 'select',
			'allow_addition' => true,
			'label'          => esc_html__( 'Chọn chiều rộng của giao diện', 'phoenixdigi' ),
			'priority'       => 51,
			'choices'        => array(
				'1000' => esc_html__( '1000px', 'phoenixdigi' ),
				'1170' => esc_html__( '1170px', 'phoenixdigi' ),
				'1200' => esc_html__( '1200px', 'phoenixdigi' ),
			),
		) );

		// Responsive settings.
		$wp_customize->add_setting( 'responsive' , array(
			'default'           => pd_default( 'responsive' ),
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'responsive', array(
			'section'  => 'title_tagline',
			'type'     => 'checkbox',
			'label'    => esc_html__( 'Bật/Tắt Responsive', 'phoenixdigi' ),
			'priority' => 52,
		) );

		// Sticky Nav Menu settings.
		$wp_customize->add_setting( 'sticky_nav_menu' , array(
			'default'           => pd_default( 'sticky_nav_menu' ),
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'sticky_nav_menu', array(
			'section'  => 'title_tagline',
			'type'     => 'checkbox',
			'label'    => esc_html__( 'Bật/Tắt Sticky Menu', 'phoenixdigi' ),
			'priority' => 53,
		) );
	}

	/**
	 * Register backgroud setting for the Theme Customizer
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_background( WP_Customize_Manager $wp_customize ) {
		// Main background settings.
		$wp_customize->add_setting( 'main_bg_color' , array(
			'default'           => pd_default( 'main_bg_color' ),
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
			'transport'         => 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_bg_color', array(
			'section'  => 'colors',
			'label'    => esc_html__( 'Chọn màu nền chủ đạo cho website', 'phoenixdigi' ),
			'priority' => 30,
		) ) );
	}

	/**
	 * Register Script setting for the Theme Customizer
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_script( WP_Customize_Manager $wp_customize ) {
		// Facebook section
		$wp_customize->add_section( 'script', array(
			'title'    => esc_html__( 'Header/Footer Script', 'phoenixdigi' ),
			'priority' => 400,
		) );

		// Header script.
		$wp_customize->add_setting( 'header_script' , array(
			'default'           => pd_default( 'header_script' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'header_script', array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Header script', 'phoenixdigi' ),
			'description' => esc_html__( 'Dán nội dung để in script lên Header.', 'phoenixdigi' ),
			'section'     => 'script',
			'priority'    => 10,
		) );

		// Allow print Header script.
		$wp_customize->add_setting( 'header_script_on_off' , array(
			'default'           => pd_default( 'header_script_on_off' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'header_script_on_off', array(
			'type'        => 'checkbox',
			'label'       => esc_html__( 'Cho phép nhúng Header script', 'phoenixdigi' ),
			'section'     => 'script',
			'priority'    => 20,
		) );

		// Footer script.
		$wp_customize->add_setting( 'footer_script' , array(
			'default'           => pd_default( 'banner_right' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'footer_script', array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Footer script', 'phoenixdigi' ),
			'description' => esc_html__( 'Dán nội dung để in script xuống Footer.', 'phoenixdigi' ),
			'section'     => 'script',
			'priority'    => 30,
		) );

		// Allow print Footer script.
		$wp_customize->add_setting( 'footer_script_on_off' , array(
			'default'           => pd_default( 'footer_script_on_off' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'footer_script_on_off', array(
			'type'        => 'checkbox',
			'label'       => esc_html__( 'Cho phép nhúng Footer script', 'phoenixdigi' ),
			'section'     => 'script',
			'priority'    => 40,
		) );
	}

	/**
	 * Register site Footer setting for the Theme Customizer
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_footer( WP_Customize_Manager $wp_customize ) {
		$wp_customize->add_section( 'footer', array(
			'title'    => esc_html__( 'Chân trang', 'phoenixdigi' ),
			'priority' => 500,
		) );

		// Site Footer settings.
		$wp_customize->add_setting( 'totop' , array(
			'default'           => pd_default( 'totop' ),
			'transport' => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'totop', array(
			'type'      => 'checkbox',
			'section'   => 'footer',
			'label'     => esc_html__( 'Bật/Tắt nút back to top?', 'phoenixdigi' ),
		) );

		$wp_customize->selective_refresh->add_partial( 'totop', array(
			'selector' => '.backtotop',
			'container_inclusive' => false,
			'render_callback' => function () {
				rt_back_to_top();
			},
		) );

		// Footer copyright setting.
		$wp_customize->add_setting( 'copyright', array(
			'default'   => pd_default( 'copyright' ),
			'transport' => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'copyright', array(
			'type'    => 'textarea',
			'section' => 'footer',
			'label'   => esc_html__( 'Copyright', 'phoenixdigi' ),
			'description' => esc_html__( 'Nội dung cuối cùng của trang web.', 'phoenixdigi' ),
		) );

		$wp_customize->selective_refresh->add_partial( 'copyright', array(
			'selector'            => '.copyright',
			'container_inclusive' => false,
			'render_callback'     => function() {
				rt_option( 'copyright' );
			},
		) );
	}

	/**
	 * Register Facebook setting for the Theme Customizer
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_facebook( WP_Customize_Manager $wp_customize ) {
		// Facebook section
		$wp_customize->add_section( 'facebook', array(
			'title'    => esc_html__( 'Facebook', 'phoenixdigi' ),
			'priority' => 600,
		) );

		// Facebook SDK JS.
		$wp_customize->add_setting( 'include_fb_sdk_js' , array(
			'default'           => pd_default( 'include_fb_sdk_js' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'include_fb_sdk_js', array(
			'type'      => 'checkbox',
			'section'   => 'facebook',
			'label'     => esc_html__( 'Nhúng Facebook SDK Js?', 'phoenixdigi' ),
		) );

		// Facebook App ID.
		$wp_customize->add_setting( 'facebook_app_id' , array(
			'default'           => pd_default( 'facebook_app_id' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'facebook_app_id', array(
			'type'      => 'text',
			'section'   => 'facebook',
			'label'     => esc_html__( 'Facebook App ID', 'phoenixdigi' ),
		) );

		// Facebook Language.
		$wp_customize->add_setting( 'fb_language', array(
			'default'           => pd_default( 'fb_language' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'fb_language', array(
			'type'    => 'select',
			'section' => 'facebook',
			'label'   => esc_html__( 'Sử dụng ngôn ngữ Facebook', 'phoenixdigi' ),
			'choices' => array(
				'vi_VN'  => esc_html__( 'Tiếng Việt', 'phoenixdigi' ),
				'en_US'  => esc_html__( 'English', 'phoenixdigi' ),

			),
		) );
	}

	/**
	 * Register Global setting for the Theme Customizer
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_general_option( WP_Customize_Manager $wp_customize ) {
		// Global panel
		$wp_customize->add_panel( 'general_option', array(
			'title'    => esc_html__( 'Cài đặt khác', 'phoenixdigi' ),
			'priority' => 800,
		) );

		// Navigation section
		$wp_customize->add_section( 'header_slider', array(
			'title'    => esc_html__( 'Header Slider', 'phoenixdigi' ),
			'priority' => 10,
			'panel'    => 'general_option',
		) );

		// Navigation setting.
		$wp_customize->add_setting( 'header_slider' , array(
			'default'           => pd_default( 'header_slider' ),
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'header_slider', array(
			'label'       => esc_html__( 'Điền Shortcode Slider', 'phoenixdigi' ),
			'panel'       => 'general_option',
			'section'     => 'header_slider',
			'priority'    => 10,
		) );
	}

	/**
	 * Sanitize raw value.
	 *
	 * @param  mixed $value Raw string value.
	 * @return string
	 */
	public static function sanitize_value( $value ) {
		return $value;
	}
}

/**
 * Fire system settings.
 */
PD_Customizer_Manager::init();
