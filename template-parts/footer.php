	<?php do_action( 'under_content_before' ); ?>

	<?php if ( is_active_sidebar( 'under-content' ) ) : ?>
	<div class="under-content-section">
		<div class="container">
			<?php dynamic_sidebar( 'under-content' ); ?>
		</div>
	</div>
	<?php endif; ?>

	<?php do_action( 'under_content_after' ); ?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">

	<?php do_action( 'before_site_footer' ); ?>

	<div class="footer_divider clearfix">
		<div class="pd-width-1-6 col"></div>
		<div class="pd-width-1-6 col"></div>
		<div class="pd-width-1-6 col"></div>
		<div class="pd-width-1-6 col"></div>
		<div class="pd-width-1-6 col"></div>
		<div class="pd-width-1-6 col"></div>
	</div>

	<div class="bottom-footer">

		<div class="container">
			<div class="footer">
				<div class="row">
					<div class="col-md-5">
						<div class="introduction color-orange">
							<h3 class="widgettitle">THÔNG TIN LIÊN HỆ</h3>
							<p>Công Ty TNHH Công Nghệ và Truyền Thông Phoexnix Digi Việt Nam chuyên cung cấp dịch vụ thiết kế website chất lượng cao, Domain, Hosting, VPS, SEO, Ads.</p>
							<p>Văn phòng: P2001, tầng 20, tòa nhà Sapphire Palace, số 4 Chính Kinh, Thanh Xuân, Hà Nội<br />
Điện thoại: 0968 716 123<br />
Email: contact@phoenixdigi.vn<br />
Website: <a href="http://phoenixdigi.vn">phoenixdigi.vn</a> - <a href="http://phoenixdigi.vn">phoenixdigi.com.vn</a><br />
<br />
STK: 0451000407829<br />
Ngân hàng: Vietcombank - CN Thành Công<br />
Chủ TK: Công Ty TNHH Công Nghệ Và Truyền Thông Phoenix Digi Việt Nam</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="clearfix">
							<h3 class="widgettitle">THÔNG TIN CHUNG</h3>
							<ul class="footer_menu list-style">
								<li><a href="/gioi-thieu/">Giới thiệu về công ty</a></li>
								<li><a href="/chinh-sach-bao-hanh/">Chính sách bảo hành</a></li>
								<li><a href="/chinh-sach-cai-dat/">Chính sách cài đặt</a></li>
								<li><a href="/chinh-sach-hoan-tien/">Chính sách hoàn tiền</a></li>
								<li><a href="/thanh-toan/">Hình thức thanh toán</a></li>
								<li><a href="/lien-he/">Liên hệ với chúng tôi</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 col-md-offset-1">
						<div class="clearfix">
							<h3 class="widgettitle">DỊCH VỤ THIẾT KẾ WEB</h3>
							<ul class="footer_menu list-style">
								<li><a href="/bang-gia-ten-mien/">Bảng giá tên miền</a></li>
								<li><a href="/bang-gia-hosting/">Bảng giá Hosting</a></li>
								<li><a href="/dich-vu-quan-tri-website/">Dịch vụ quản trị website</a></li>
								<li><a href="dich-vu-marketing-online/">Dịch vụ marketing online</a></li>
								<li><a href="/thiet-ke-web-chuan-seo/">Thiết kế web chuẩn SEO</a></li>
								<li><a href="/thiet-ke-web-mien-phi/">Thiết kế website miễn phí</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-7 color-orange">Sử dụng nội dung ở trang này và dịch vụ tại <a href="<?php echo esc_url( site_url( '/' ) ); ?>">Phoenix Digi</a> nghĩa là bạn đồng ý với <a href="<?php echo esc_url( site_url( '/thoa-thuan-su-dung/' ) ); ?>">Thỏa thuận sử dụng</a> và <a href="<?php echo esc_url( site_url( '/chinh-sach-bao-mat-thong-tin/' ) ); ?>">Chính sách bảo mật thông tin</a> của chúng tôi.</div>
							<div class="col-md-3">
								<a href="#" title="Thông báo bộ công thương" target="_blank" rel="nofollow" id="signedBCT"><img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/dathongbao.png' ) ); ?>" alt="Thông báo bộ công thương"></a><br><br>
<a href="#" title="DMCA.com Protection Status" class="dmca-badge" rel="nofollow"><img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/dmca.png' ) ); ?>" alt="DMCA.com Protection Status"></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-4">
								<div class="footer_socials">
									<h4 class="widgettitle">MẠNG XÃ HỘI</h4>
									<p>Tìm Phoenix Digi trên các mạng xã hội</p>
									<div class="footer_socials-list clearfix">
										<a href="http://fb.com/phoenixdigi.vn" target="_blank"><i class="fa fa-facebook"></i></a>
										<a href="#"><i class="fa fa-twitter" target="_blank"></i></a>
										<a href="#"><i class="fa fa-google-plus" target="_blank"></i></a>
										<a href="#"><i class="fa fa-linkedin" target="_blank"></i></a>
										<a href="#"><i class="fa fa-instagram" target="_blank"></i></a>
										<a href="#"><i class="fa fa-pinterest" target="_blank"></i></a>
									</div>
								</div>
							</div>
							<div class="col-md-7 col-md-offset-1">
								<div class="footer_email">
									<h4 class="widgettitle">NHẬN TIN MỚI</h4>
									<p>Để lại Email của bạn nhận tin khuyến mãi từ Phoenix Digi Việt Nam</p>
									<?php echo do_shortcode( '[contact-form-7 id="14" title="Liên hệ chân trang"]' ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<p>&copy; 2017 <a href="<?php echo esc_url( home_url( '/' ) ); ?>">PHOENIX DIGI VIET NAM</a>. ALL RIGHTS RESERVED. DESIGNED BY <a href="http://www.phoenixdigi.vn">PHOENIX DIGI VIET NAM</a></p>
			</div>
		</div>

	</div>

	<?php do_action( 'after_site_footer' ); ?>

</footer><!-- #colophon -->

<?php get_template_part( 'template-parts/mobile-menu' ); ?>

<?php if ( pd_option( 'totop', true, false ) ) : ?>
	<div class="backtotop"><?php pd_back_to_top(); ?></div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'fixed-area' ) ) : ?>
	<div class="fixed-area">
		<?php dynamic_sidebar( 'fixed-area' ); ?>
	</div>
<?php endif; ?>