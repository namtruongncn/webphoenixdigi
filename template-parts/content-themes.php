<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

?>
<div class="col-md-4 fix-guiter">
	<div class="card cart-block">
		<div class="card-img">
			<a href="<?php the_post_thumbnail_url( 'full' ); ?>">
				<?php the_post_thumbnail( 'full' ); ?>
			</a>
			<div class="overlay-images">
				<div class="preset">
					<div class="info">
						<div class="info-price">
							<?php
								$metadata = get_post_meta( get_the_ID(), 'themes', true );

								$price = '';
								if ( $metadata ) {
									$price = $metadata['price'];
								}

								$link = '';
								if ( $metadata ) {
									$link = $metadata['link'];
								}

								$image = '';
								if ( $metadata ) {
									$image = $metadata['image'];
								}

								printf( esc_html__( 'Giá: %s VNĐ' ), '<span>' . $price . '</span>' );
							?>
						</div>
						<?php the_title( '<div class="info-title"><h3>', '</h3></div>' ); ?>
					</div>
					<?php if ( $image ) : ?>
						<a href="<?php echo wp_get_attachment_image_url( $image, 'full' ); ?>" target="_blank" class="btn style-btn" data-fancybox data-caption="<?php the_title(); ?>">
							<i class="fa fa-external-link" aria-hidden="true"></i>
							<span class="text">Ảnh Demo</span>
						</a>
					<?php endif; ?>
					<?php if ( $link ) : ?>
						<a href="<?php echo esc_url( $link ); ?>" target="_blank" class="view-link btn style-btn">
							<i class="fa fa-external-link" aria-hidden="true"></i>
							<span class="text">Web Demo</span>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>