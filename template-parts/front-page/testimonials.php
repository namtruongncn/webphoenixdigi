<div id="testimonials" class="testimonials">
	<div class="container">
		<div class="row">
			<div class="content-title">
				<h2>Đánh giá khách hàng về Phoenix Digi Việt Nam</h2>
			</div>
			<div class="testimonial col-md-4 col-sm-6 col-xs-12">
				<blockquote>Vừa là khách hàng sử dụng dịch vụ website vừa là đối tác chính thức, tôi rất vinh dự và hài lòng khi hợp tác với Phoenix Digi Việt Nam.</blockquote>
				<div class="people-info">
					<!-- <div class="photo">
						<img src="<?php // echo get_theme_file_uri( 'assets/images/hoaiminh.jpg' ); ?>" alt="">
					</div> -->
					<div class="people-text"><p>HOÀI MINH</p>Trưởng phòng kỹ thuật - Techhost.vn</div>
				</div>
			</div>
			<!-- .testimonial -->
			<div class="testimonial col-md-4 col-sm-6 col-xs-12">
				<blockquote>Cảm ơn Phoenix Digi Việt Nam đã cung cấp cho tôi một Website thật đẹp và các chức năng tuyệt vời giúp tôi bán hàng thuận lợi.</blockquote>
				<div class="people-info">
					<div class="people-text"><p>THU TRANG</p>Chủ shop bán hàng online</div>
				</div>
			</div>
			<!-- .testimonial -->
			<div class="testimonial col-md-4 col-sm-6 col-xs-12">
				<blockquote>Thời đại công nghệ số không những cần có Website đẹp, chức năng tốt và còn phải tối ưu SEO. Phoenix Digi Việt Nam đã cho tôi tất cả.</blockquote>
				<div class="people-info">
					<div class="people-text"><p>VIỆT ANH</p>Nhiếp ảnh gia - aocuoivietanh.com</div>
				</div>
			</div>
			<!-- .testimonial -->
			<div class="testimonial col-md-4 col-sm-6 col-xs-12">
				<blockquote>Thật sự tôi phải nói một điều rằng các bạn rất tuyệt vời, tôi đã yêu các bạn ngay sau khi nhận bản thiết kế logo và bộ nhận diện thương hiệu.</blockquote>
				<div class="people-info">
					<div class="people-text"><p>MẠNH TIẾN</p>Kinh doanh bất động sản</div>
				</div>
			</div>
			<!-- .testimonial -->
			<div class="testimonial col-md-4 col-sm-6 col-xs-12">
				<blockquote>Tôi có một website thật tuyệt vời, lợi nhuận của tôi tăng vọt khi đăng ký thiết kế website với Phoenix Digi Việt Nam.</blockquote>
				<div class="people-info">
					<div class="people-text"><p>PHẠM HƯƠNG</p>Thiết kế thời trang</div>
				</div>
			</div>
			<!-- .testimonial -->
			<div class="testimonial col-md-4 col-sm-6 col-xs-12">
				<blockquote>Website của tôi đã chạy tốt hơn, không còn vấn đề trục trặc như trước, công việc kinh doanh của tôi nhờ thế mà phát triển hơn.</blockquote>
				<div class="people-info">
					<div class="people-text"><p>NGUYỄN QUÂN</p>Kinh doanh tự do</div>
				</div>
			</div>
			<!-- .testimonial -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</div>