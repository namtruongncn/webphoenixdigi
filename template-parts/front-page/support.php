<?php
/**
 * Support template.
 */
?>

<!-- END SUPPORT -->
<section class="support">
	<div class="container">
		<div class="row">
			<div class="content-title">
				<h2>Các tư vấn viên sẵn sàng hỗ trợ bạn</h2>
			</div>
			<ul class="list-unstyled clearfix">
				<li class="support-item col-md-3 col-sm-6 col-xs-12 text-center">
					<img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/namtruong.jpg' ) ); ?>" width="200px" height="200px" alt="">
					<h4 class="support-item-title">Nam Trương</h4>
					<div class="support-item-phone"><a href="tel:01639482057"><i class="fa fa-phone"></i> 01639.482.057</a></div>
					<h5 class="support-item-desc">Hỗ trợ kỹ thuật</h5>
				</li>
				<li class="support-item col-md-3 col-sm-6 col-xs-12 text-center">
					<img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/nganhoang.jpg' ) ); ?>" width="200px" height="200px" alt="">
					<h4 class="support-item-title">Hoàng Ngân</h4>
					<div class="support-item-phone"><a href="tel:0974201866"><i class="fa fa-phone"></i> 0974.201.866</a></div>
					<h5 class="support-item-desc">TƯ VẤN CHUYÊN SÂU</h5>
				</li>
				<li class="support-item col-md-3 col-sm-6 col-xs-12 text-center">
					<img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/thuyle.jpg' ) ); ?>" width="200px" height="200px" alt="">
					<h4 class="support-item-title">Lê Thúy</h4>
					<div class="support-item-phone"><a href="tel:0978125966"><i class="fa fa-phone"></i> 0978.125.966</a></div>
					<h5 class="support-item-desc">TƯ VẤN CHUYÊN SÂU</h5>
				</li>
				<li class="support-item col-md-3 col-sm-6 col-xs-12 text-center">
					<img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/daothao.jpg' ) ); ?>" width="200px" height="200px" alt="">
					<h4 class="support-item-title">Đào Thao</h4>
					<div class="support-item-phone"><a href="tel:0984638060"><i class="fa fa-phone"></i> 098.463.8060</a></div>
					<h5 class="support-item-desc">Tư vấn chung</h5>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- END SUPPORT -->