<?php
/**
 * Search Domain Template.
 */
?>

<!-- REGISTER_DOMAIN_NAME -->
<section class="check-domain">
	<div class="container-domain">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-md-offset-3">
					<div class="title-check">
						<h2>Đăng ký tên miền để bảo vệ thương hiệu của bạn</h2>
					</div>
					<div class="dot-name">
						<span>.com</span>
						<span>.net</span>
						<span>.org</span>
						<span>.vn</span>
						<span>.com.vn</span>
						<span>.net.vn</span>
						<span>.edu.vn</span>
					</div>
					<div class="box-search box-domain-search">
						<form class="form-inline">
							<div class="form-group">
								<input name="name" type="text" class="form-control domain-name" id="" placeholder="tenmiencuaban.com">
							</div>
							<button type="submit" class="btn btn-search"><i class="fa fa-search" aria-hidden="true"></i>Kiểm tra</button>
						</form>
						<div class="loading hide"><img src="<?php echo get_theme_file_uri( 'assets/images/loading-bar.gif' ); ?>" alt=""></div>
						<div id="result"></div>
					</div>
				</div>
				</div>
		</div>
	</div>
</section>
<!-- END REGISTER_DOMAIN_NAME -->