<?php
/**
 * Template for displaying search forms in PD Theme
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */
?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<span class="screen-reader-text"><?php echo _x( 'Tìm kiếm cho:', 'label', 'phoenixdigi' ); ?></span>
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder= "<?php esc_attr_e( 'Nhập từ khóa tìm kiếm...', 'pd-theme' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit"><i class="fa fa-search" aria-hidden="true"></i><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'phoenixdigi' ); ?></span></button>
</form>
