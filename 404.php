<?php
/**
 * Trang chủ template
 * Description: Template for home page.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

get_header( 'fullwidth' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<div class="container">
					<header class="page-header">
						<h1 class="page-404-title"><?php esc_html_e( 'Xin lỗi, Không tìm thấy trang bạn tìm kiếm.', 'phoenixdigi' ); ?></h1>
					</header><!-- .page-header -->
					<div class="page-content">
						<p><?php _e( 'Hãy thử tìm kiếm một trang khác!', 'phoenixdigi' ); ?></p>

						<?php get_search_form(); ?>

						<h5>Hoặc quay về <a href="<?php echo esc_url( site_url() ); ?>"><?php esc_html_e( 'TRANG CHỦ', 'phoenixdigi' ); ?></a></h5>

					</div><!-- .page-content -->
				</div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer( 'fullwidth' );
